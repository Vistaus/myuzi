��    0      �  C         (     )     /  '   ?     g     �     �     �     �     �     �     �     �                 	   5     ?     G  	   W     a          �     �     �     �     �     �  !   �          +     B  	   Y     c     q     �     �     �     �     �     �     �               0     =     I     P  �   `     _     l  =   �  (   �  )   �     	     %	  	   .	     8	     K	     i	     ~	  	   �	     �	     �	     �	  	   �	     �	     �	  (   �	     
     &
     -
  "   3
     V
     m
  %   �
  )   �
  $   �
  "   �
          <     E     X     u     �     �     �     �  $   �  
   �       $   "     G     ^     n     u        /                            #                                         &   +                
          .           (       -      	   $       )   0            '                                       !   ,   %             *   "        About Add to playlist Autoplay similar songs after queue ends Can't search in offline mode Can't switch to online mode Connection failed Delete Donate Download error Downloading playlist... Downloading song... Export to clipboard Exported Import Import from clipboard Move down Move up New playlist... Next song No playlist data in clipboard Offline Online Play Play random songs from queue Playback error Playlist already exists Playlist creation failed Playlist data copied to clipboard Playlist download failed Playlist import failed Playlist rename failed Playlists Previous song Remove from local storage Remove from playlist Repeat this song Report a bug Save to local storage Search Searching for a song to play... Searching... Song download failed Toggle online mode Toggle pause Unavailable Volume | Contributors: Project-Id-Version: 
PO-Revision-Date: 
Last-Translator: Albano Battistella <albano_battistella@hotmail.com>
Language-Team: 
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.1.1
 Informazioni Aggiungi a playlist Riproduzione automatica di brani simili al termine della coda Impossibile cercare in modalità offline Impossibile passare alla modalità online Connessione fallita Cancella Donazione Errore di download Download playlist in corso... Download in corso... Esporta negli appunti Esportato Importa Importa dagli appunti Sposta giù Sposta su Nuova playlist... Prossima canzone Nessun dato della playlist negli appunti Offline Online Avvia Riproduci brani casuali dalla coda Errore di riproduzione La playlist esiste già Creazione della playlist non riuscita Dati della playlist copiati negli appunti Download della playlist non riuscito Importazione playlist non riuscita Rinomina playlist non riuscita Playlist Canzone precedente Rimuovi dalla memoria locale Rimuovi dalla playlist Ripeti questa canzone Segnala un bug Salva nella memoria locale Cerca Ricerca di un brano da riprodurre... Ricerca... Download del brano non riuscito Attiva/disattiva la modalità online Attiva/disattiva pausa Non disponibile Volume | Contributori: 