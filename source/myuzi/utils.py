import concurrent.futures, json, os, random, subprocess

import bs4, requests
from PIL import Image
from gi.repository import GLib


### --- NETWORK FUNCTIONS --- ###


def is_yt_available() -> bool:
	try:
		return requests.head('https://www.youtube.com').status_code == 200
	except requests.exceptions.ConnectionError:
		return False


# returns song uri given yt video id
def get_song_uri(video_id: str) -> str:
	local_path = GLib.get_user_special_dir(
		GLib.UserDirectory.DIRECTORY_MUSIC
	) + '/myuzi/' + video_id
	if os.path.exists(local_path):
		return 'file://' + local_path

	# --no-cache-dir might help with 403 errors
	out, _ = subprocess.Popen(
		f'yt-dlp -g -x --no-cache-dir https://www.youtube.com/watch?v={video_id}',
		shell = True,
		stdout = subprocess.PIPE
	).communicate()

	return out.decode().split('\n')[0]


def clear_thumbnails():
	path = os.getenv(
		'XDG_CONFIG_HOME',
		os.path.expanduser('~/.config')
	) + '/myuzi/thumbs/'
	os.makedirs(path, exist_ok = True)

	for f in os.listdir(path):
		try:
			os.remove(path + f)
		except (IsADirectoryError, FileNotFoundError):
			pass


# saves thumbnail and crops to square
def save_song_thumbnail(video_id: str, thumb_url: str):
	path = os.getenv(
		'XDG_CONFIG_HOME',
		os.path.expanduser('~/.config')
	) + '/myuzi/thumbs/'
	os.makedirs(path, exist_ok = True)

	with open(path + video_id, 'wb') as thumb:
		thumb.write(requests.get(thumb_url).content)

	img = Image.open(path + video_id)
	margin = (img.width - img.height) / 2
	img = img.crop((margin, 0, margin + img.height, img.height))
	img.save(path + video_id, 'PNG')


def get_similar_song(video_id: str, ignore: list = None) -> dict | None:
	ignore = [] if ignore == None else ignore

	# not using yt's api because limits
	try:
		text = requests.get('https://www.youtube.com/watch?v=' + video_id).text
	except requests.exceptions.ConnectionError:
		return None

	soup = bs4.BeautifulSoup(text, 'html.parser')

	# extract video data from json in a js variable
	data = {}
	for script in soup.find_all('script'):
		text = script.text.strip()
		keystring = 'var ytInitialData ='
		if text.startswith(keystring):
			data = json.loads(text.replace(keystring, '').strip(';'))
			break

	vids = data['contents']['twoColumnWatchNextResults']['secondaryResults']['secondaryResults']['results']
	random.shuffle(vids)

	for video in vids:
		# videos only
		if not 'compactVideoRenderer' in video:
			continue

		metadata = video['compactVideoRenderer']

		# skip ids on ignore list
		if metadata['videoId'] in ignore:
			continue

		# ignore long videos
		length = metadata['lengthText']['simpleText'].split(':')
		if len(length) > 2:
			continue # more than 1 hour
		if int(length[0]) > 9:
			continue # more than 9 minutes

		return {
			'title': metadata['title']['simpleText'],
			'author': metadata['longBylineText']['runs'][0]['text'],
			'id': metadata['videoId']
		}

	return None


# searches yt and returns parsed results
def find_songs(query: str) -> list:
	clear_thumbnails()

	# not using yt's api because limits
	try:
		text = requests.get('https://www.youtube.com/results?search_query=' + query).text
	except requests.exceptions.ConnectionError:
		return []

	# extract video data from json in a js variable
	soup = bs4.BeautifulSoup(text, 'html.parser')
	data = {}
	for script in soup.find_all('script'):
		text = script.text.strip()
		keystring = 'var ytInitialData ='
		if text.startswith(keystring):
			data = json.loads(text.replace(keystring, '').strip(';'))
			break

	contents = (
		data['contents']
		['twoColumnSearchResultsRenderer']
		['primaryContents']
		['sectionListRenderer']
		['contents']
	)

	# find the part that contains the videos
	vids = []
	for child in contents:
		for item in child['itemSectionRenderer']['contents']:
			if 'videoRenderer' in item:
				break
		else: # nobreak
			continue # keep checking

		# has videoRenderer children, so pick this one
		vids = child['itemSectionRenderer']['contents']
		break
	else: # nobreak - no videoRenderers found
		return []

	# parse song data
	songs = []
	for song in vids:
		# ignore channels, playlists
		if not 'videoRenderer' in song:
			continue

		metadata = song['videoRenderer']

		# ignore livestreams
		live = False
		if 'badges' in metadata:
			for badge in metadata['badges']:
				if badge['metadataBadgeRenderer']['style'] == 'BADGE_STYLE_TYPE_LIVE_NOW':
					live = True
					break

		# not normal videos - livestreams or something
		if live or 'lengthText' not in metadata:
			continue

		# check if official artist channel or verified channel
		verified = False
		official = False
		if 'ownerBadges' in metadata:
			for badge in metadata['ownerBadges']:
				badge_style = badge['metadataBadgeRenderer']['style']
				if badge_style == 'BADGE_STYLE_TYPE_VERIFIED_ARTIST':
					official = True
				elif badge_style == 'BADGE_STYLE_TYPE_VERIFIED':
					verified = True

		songs.append({
			'title': metadata['title']['runs'][0]['text'],
			'author': metadata['ownerText']['runs'][0]['text'],
			'thumbnail': metadata['thumbnail']['thumbnails'][0]['url'],
			'length': metadata['lengthText']['simpleText'],
			'id': metadata['videoId'],
			'official': official,
			'verified': verified
		})

	# async thumbnail downloads
	with concurrent.futures.ThreadPoolExecutor() as executor:
		tasks = {
			executor.submit(
				save_song_thumbnail, s['id'], s['thumbnail']
			): s for s in songs
		}

		# await completion
		for future in concurrent.futures.as_completed(tasks):
			pass

	# improved sort
	def sort_key(song: dict) -> int:
		# high bias for verified/official channels
		score = 5 * song['verified'] + 10 * song['official']

		# one point per matching word in query
		for word in query.lower().split(' '):
			if word in song['title'].lower():
				score += 1
			if word in song['author'].lower():
				score += 1

		# of course lowest numbers go first in sorting
		return -score

	songs.sort(key = sort_key)
	return songs

