��    *      l  ;   �      �     �     �  '   �     �               /     =     D     K     Z     r     �     �     �     �  	   �     �     �  -   �               &     5  !   M     o     �     �  	   �     �     �     �  	             !     (     H     U     j     v     }  \  �     �     �  E     *   V  3   �     �     �     �     �     �  (   	     :	     Y	  !   w	  
   �	  "   �	     �	     �	     �	  N   
     \
     c
     k
  !   �
  3   �
  -   �
  .   	  -   8     f  "   w  !   �     �     �     �     �  C        F  '   W          �  .   �            #   "          &              	       (                             *             %      
   $   )                                !                                                  '                About Add to queue Autoplay similar songs after queue ends Can't search in offline mode Can't switch to online mode Connection failed Copy song URL Delete Donate Download error Downloading playlist... Downloading song... Enter name... Export to clipboard Exported Import from clipboard Move down Move up New playlist... No playlist data or playlist URL in clipboard Online Patrons Playback error Playlist already exists Playlist data copied to clipboard Playlist download failed Playlist import failed Playlist rename failed Playlists Remove from local storage Remove from playlist Remove from queue Rename... Save to local storage Search Searching for a song to play... Searching... Song download failed Unavailable Volume translator-credits Project-Id-Version: myuzi
PO-Revision-Date: 
Last-Translator: Jürgen Benvenuti <gastornis@posteo.org>
Language-Team: German <gnome-de@gnome.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.1.1
X-Poedit-SourceCharset: UTF-8
 Info Zur Warteschlange hinzufügen Automatisch ähnliche Titel wiedergeben, wenn die Warteschlange endet Im Offline-Modus kann nicht gesucht werden Es kann nicht in den Online-Modus gewechselt werden Verbindung fehlgeschlagen Titel-Adresse kopieren Löschen Spenden Fehler beim Herunterladen Wiedergabeliste wird heruntergeladen … Titel wird heruntergeladen … Geben Sie einen Namen ein … In die Zwischenablage exportieren Exportiert Aus der Zwischenablage importieren Nach unten verschieben Nach oben verschieben Neue Wiedergabeliste … Keine Wiedergabeliste-Daten oder Wiedergabeliste-Adresse in der Zwischenablage Online Patrons Fehler bei der Wiedergabe Wiedergabeliste existiert bereits Wiedergabeliste-Daten in die Zwischenablage kopiert Fehler beim Herunterladen der Wiedergabeliste Importieren der Wiedergabeliste fehlgeschlagen Umbenennen der Wiedergabeliste fehlgeschlagen Wiedergabelisten Aus dem lokalen Speicher entfernen Aus der Wiedergabeliste entfernen Aus der Warteschlange entfernen Umbenennen … Lokal speichern Suchen Es wird nach einem Titel gesucht, der wiedergegeben werden kann … Suche läuft … Herunterladen des Titels fehlgeschlagen Nicht verfügbar Lautstärke Jürgen Benvenuti <gastornis@posteo.org>, 2022 