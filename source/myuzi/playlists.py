import base64, binascii, json, os

import bs4, requests


# playlists = {
#	'my playlist': [
#		{'id': 'ASvGDFQwe', 'title': 'Cool song'}
#	]
# }


### --- PLAYLIST FUNCTIONS --- ###


def add_playlist(name: str, songs: dict = None) -> bool:
	new_lists = read_playlists()

	# append (n) to playlist name to keep them unique
	if name in new_lists:
		i = 1
		while f'{name} ({str(i)})' in new_lists:
			i += 1

		name = f'{name} ({str(i)})'

	new_lists[name] = songs if songs else []
	write_playlists(new_lists)


def rename_playlist(name: str, new_name: str) -> bool:
	new_lists = read_playlists()
	if new_name not in new_lists:
		new_lists[new_name] = new_lists.pop(name)
		write_playlists(new_lists)
		return True

	return False


# creates playlist from data or yt url
def import_playlist(name: str, data: str) -> bool:
	new_lists = read_playlists()

	def get_single_song(url: str) -> list:
		# schema required for requests
		if not url.startswith('http'):
			url = 'https://' + url

		try:
			text = requests.get(url).text
		except:
			return []

		soup = bs4.BeautifulSoup(text, 'html.parser')

		# extract song data from json in a js variable
		data = {}
		for script in soup.find_all('script'):
			text = script.text.strip()
			keystring = 'var ytInitialData ='
			if keystring in text:
				data = json.loads(text.split(keystring)[1].strip(';'))
				break

		# parse json respopnse
		try:
			# single song?
			return [{
				'id': data['currentVideoEndpoint']['watchEndpoint']['videoId'],
				'title': data['contents']['twoColumnWatchNextResults']['results']['results']['contents'][0]['videoPrimaryInfoRenderer']['title']['runs'][0]['text']
			}]
		except (KeyError, TypeError):
			return []

	# returns songs from playlist url in range (song -> api limit)
	def get_songs_from(url: str, from_song: str, from_i: int) -> list:
		# schema required for requests
		if not url.startswith('http'):
			url = 'https://' + url

		# playlist url conversion
		url = url.replace('/playlist?', '/watch?')

		# from this song onward
		if from_song and from_i:
			url += '&v=' + from_song

		try:
			text = requests.get(url).text
		except:
			return []

		soup = bs4.BeautifulSoup(text, 'html.parser')

		# extract playlist data from json in a js variable
		data = {}
		for script in soup.find_all('script'):
			text = script.text.strip()
			keystring = 'var ytInitialData ='
			if keystring in text:
				data = json.loads(text.split(keystring)[1].strip(';'))
				break

		# parse json respopnse
		try:
			songs = data['contents']['twoColumnWatchNextResults']['playlist']['playlist']['contents']
		except (KeyError, TypeError):
			return []

		# build playlist
		playlist = []
		for song in songs:
			try:
				song = song['playlistPanelVideoRenderer']
			except KeyError:
				continue

			# start at song
			if int(song['navigationEndpoint']['watchEndpoint']['index']) < from_i:
				continue

			playlist.append({
				'id': song['videoId'],
				'title': song['title']['simpleText'],
			})

		return playlist

	# interpret as b64 enc data, otherwise as url
	try:
		playlist = json.loads(
			base64.urlsafe_b64decode(data.encode()).decode()
		)
	except (json.decoder.JSONDecodeError, binascii.Error, UnicodeDecodeError):
		if not ('/playlist?' in data or '&list=' in data or '?list=' in data):
			playlist = get_single_song(data)
		else:
			playlist = []
			while True:
				batch = get_songs_from(
					data,
					playlist[-1]['id'] if playlist else '',
					len(playlist)
				)

				if not batch:
					break
				playlist += batch

	# append (n) to playlist name to keep them unique
	if name in new_lists:
		i = 1
		while f'{name} ({str(i)})' in new_lists:
			i += 1

		name = f'{name} ({str(i)})'

	if playlist:
		new_lists[name] = playlist
		write_playlists(new_lists)
		return True

	return False


# returns a b64-string representation of a playlist's contents
def export_playlist(name: str) -> str:
	return base64.urlsafe_b64encode(
		json.dumps(read_playlists()[name]).encode()
	).decode()


def remove_playlist(name: str):
	new_lists = read_playlists()
	new_lists.pop(name)
	write_playlists(new_lists)


### --- SONG FUNCTIONS --- ###


def is_song_in_any_playlist(id_: str) -> bool:
	for playlist in read_playlists().values():
		for song in playlist:
			if song['id'] == id_:
				return True

	return False


def add_song(id_: str, title: str, playlist: str):
	new_lists = read_playlists()
	new_lists[playlist].append({'title': title, 'id': id_})
	write_playlists(new_lists)


def rename_song(index: int, playlist: str, new_name: str):
	new_lists = read_playlists()
	new_lists[playlist][index]['title'] = new_name
	write_playlists(new_lists)


def swap_songs(i: int, j: int, playlist: str):
	lists = read_playlists()
	lists[playlist][i], lists[playlist][j] = lists[playlist][j], lists[playlist][i]
	write_playlists(lists)


def remove_song(index: int, playlist: str):
	new_lists = read_playlists()
	new_lists[playlist].pop(index)
	write_playlists(new_lists)


### --- UTILITY FUNCTIONS --- ###


def write_playlists(playlists: dict):
	dir_path = os.getenv('XDG_CONFIG_HOME', os.path.expanduser('~/.config')) + '/myuzi'
	lists_path = dir_path  + '/playlists.json'

	try:
		with open(str(lists_path), 'w') as lists_file:
			lists_file.write(json.dumps(playlists))
	except FileNotFoundError:
		os.makedirs(str(dir_path))
		write_playlists(playlists)


def read_playlists() -> dict:
	lists_path = os.getenv('XDG_CONFIG_HOME', os.path.expanduser('~/.config')) + '/myuzi/playlists.json'

	try:
		with open(lists_path, 'r') as lists_file:
			return json.loads(lists_file.read())
	except OSError:
		return {}

