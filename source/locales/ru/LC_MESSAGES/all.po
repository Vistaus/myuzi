msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: ru_RU\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);\n"
"X-Generator: Poedit 3.2.1\n"

#: ../myuzi/app.py:146
msgid "Searching for a song to play..."
msgstr "Поиск песни для воспроизведения..."

#: ../myuzi/app.py:204
msgid "Enter name..."
msgstr "Введите название..."

#: ../myuzi/app.py:272
msgid "Playback error"
msgstr "Ошибка воспроизведения"

#: ../myuzi/app.py:485
msgid "Downloading song..."
msgstr "Загрузка песни..."

#: ../myuzi/app.py:501 ../myuzi/app.py:676
msgid "Download error"
msgstr "Ошибка загрузки"

#: ../myuzi/app.py:502
msgid "Song download failed"
msgstr "Загрузка песни не удалась"

#: ../myuzi/app.py:581
msgid "Playlist import failed"
msgstr "Сбой импорта списка воспроизведения"

#: ../myuzi/app.py:582
msgid "No playlist data or playlist URL in clipboard"
msgstr "Отсутствуют данные списка воспроизведения или URL-адреса списка воспроизведения в буфере обмена"

#: ../myuzi/app.py:611
msgid "Playlist rename failed"
msgstr "Не удалось переименовать плейлист"

#: ../myuzi/app.py:612
msgid "Playlist already exists"
msgstr "Список воспроизведения уже существует"

#: ../myuzi/app.py:633
msgid "Exported"
msgstr "Экспортировано"

#: ../myuzi/app.py:634
msgid "Playlist data copied to clipboard"
msgstr "Данные списка воспроизведения скопированы в буфер обмена"

#: ../myuzi/app.py:653
msgid "Downloading playlist..."
msgstr "Загрузка списка воспроизведения..."

#: ../myuzi/app.py:677
msgid "Playlist download failed"
msgstr "Не удалось загрузить плейлист"

#: ../myuzi/app.py:727
msgid "Unavailable"
msgstr "Недоступно"

#: ../myuzi/app.py:728
msgid "Can't search in offline mode"
msgstr "Невозможно выполнить поиск в автономном режиме"

#: ../myuzi/app.py:750
msgid "Searching..."
msgstr "Поиск..."

#: ../myuzi/app.py:771
msgid "Can't switch to online mode"
msgstr "Невозможно переключиться в онлайн-режим"

#: ../myuzi/app.py:772
msgid "Connection failed"
msgstr "Ошибка подключения"

#: ../myuzi/app.py:798
msgid "translator-credits"
msgstr ""

#: ../myuzi/app.py:799
msgid "Patrons"
msgstr "Покровители"

#: ../myuzi/app.py:801
msgid "Donate"
msgstr "Пожертвовать"

#: ../myuzi/app.py:936 ../myuzi/app.py:1141
msgid "New playlist..."
msgstr "Новый плейлист..."

#: ../myuzi/app.py:942 ../myuzi/app.py:1059
msgid "Add to queue"
msgstr "Добавить в очередь"

#: ../myuzi/app.py:1004
msgid "Delete"
msgstr "Удалить"

#: ../myuzi/app.py:1007 ../myuzi/app.py:1075
msgid "Remove from local storage"
msgstr "Удалить из локального хранилища"

#: ../myuzi/app.py:1010
msgid "Export to clipboard"
msgstr "Экспорт в буфер обмена"

#: ../myuzi/app.py:1013 ../myuzi/app.py:1055
msgid "Rename..."
msgstr "Переименовать..."

#: ../myuzi/app.py:1019 ../myuzi/app.py:1080
msgid "Save to local storage"
msgstr "Сохранить в локальное хранилище"

#: ../myuzi/app.py:1065
msgid "Move up"
msgstr "Переместить вверх"

#: ../myuzi/app.py:1070
msgid "Move down"
msgstr "Переместить вниз"

#: ../myuzi/app.py:1086
msgid "Remove from playlist"
msgstr "Удалить из списка воспроизведения"

#: ../myuzi/app.py:1197
msgid "Remove from queue"
msgstr "Удалить из очереди"

#: ../myuzi/app.py:1202
msgid "Copy song URL"
msgstr "Скопировать URL песни"

#: ../myuzi/app.py:1207
msgid "Volume"
msgstr "Громкость"

#: ../myuzi/app.py:1228
msgid "Autoplay similar songs after queue ends"
msgstr "Автовоспроизведение похожих песен после окончания очереди"

#: ../myuzi/app.py:1308
msgid "Playlists"
msgstr "Плейлисты"

#: ../myuzi/app.py:1309
msgid "Search"
msgstr "Поиск"

#: ../myuzi/app.py:1315
msgid "Online"
msgstr "Онлайн"

#: ../myuzi/app.py:1320
msgid "Import from clipboard"
msgstr "Импорт из буфера обмена"

#: ../myuzi/app.py:1325
msgid "About"
msgstr "О Программе"
