Myuzi is a free and open source Linux app for streaming music from YouTube. It has no ads and does not require an account.

Uses SponsorBlock data licensed used under CC BY-NC-SA 4.0 from https://sponsor.ajay.app/.

Copyright © 2022 zehkira, [MIT License](https://gitlab.com/zehkira/myuzi/-/blob/master/source/LICENSE).

| [Download](https://gitlab.com/zehkira/myuzi/-/blob/master/INSTALL.md#install-package) | [Donate](https://www.patreon.com/bePatron?u=65739770) |
|-|-|

<img src='https://gitlab.com/zehkira/myuzi/-/raw/master/assets/screenshot2.png' alt='screenshot'>
