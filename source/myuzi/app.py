import logging, os

from myuzi import __version__
import myuzi.cache
import myuzi.player
import myuzi.playlists
import myuzi.settings
import myuzi.sponsorblock
import myuzi.utils

import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Gdk, GLib, Pango, Adw


### --- DATA CLASSES --- ###


class App:
	player = myuzi.player.Player()
	downloading: bool = False
	searching: bool = False
	search_results: list = []


class Interface:
	collapsed_playlists = [name for name in myuzi.playlists.read_playlists()]


### --- MISC FUNCTIONS --- ###


def update_state() -> True:
	if App.player.is_busy():
		Interface.window.set_sensitive(False)
		return True

	# song title
	song = App.player.get_current_song()
	Interface.lbl_title.set_text(song['title'] if song else '...')

	# playback status
	Interface.scl_progress.set_value(App.player.get_progress())
	Interface.btn_pause.set_icon_name(
		'media-playback-start' if App.player.is_paused() else 'media-playback-pause'
	)

	if not (App.downloading or App.searching):
		Interface.window.set_sensitive(True)

	return True


def show_rename_popup(callback, name: str):
	def _on_submit_popup(entry: Gtk.Entry, popup: Adw.Window, callback):
		name = entry.get_text()
		if name:
			callback(name)
			popup.destroy()

	def _on_cancel_popup(btn: Gtk.Button, popup: Adw.Window):
		popup.destroy()

	btn = Gtk.Button.new_from_icon_name('edit-undo')

	entry = Gtk.Entry.new()
	entry.set_text(name)
	entry.set_placeholder_text(_('Enter name...'))

	box = Gtk.Box(orientation = Gtk.Orientation.HORIZONTAL)
	box.set_spacing(5)
	box.set_margin_bottom(5)
	box.set_margin_top(5)
	box.set_margin_start(5)
	box.set_margin_end(5)
	box.append(btn)
	box.append(entry)

	popup = Adw.Window.new()
	popup.set_transient_for(Interface.window)
	popup.set_modal(True)
	popup.set_resizable(False)
	popup.set_content(box)
	btn.connect('clicked', _on_cancel_popup, popup)
	entry.connect('activate', _on_submit_popup, popup, callback)
	entry.grab_focus_without_selecting()
	popup.show()


### --- EVENT FUNCTIONS --- ###


def _on_exit(_win: Adw.ApplicationWindow):
	App.player.terminate()


# play/pause button click event
def _on_toggle_pause(_):
	App.player.toggle_pause()


# next song button click event
def _on_next_song(_):
	GLib.Thread.new(None, App.player.next_song)


# previous song button click event
def _on_previous_song(_):
	GLib.Thread.new(None, App.player.previous_song)


# play queue starting at index
def _on_play_queue(_, queue: list, index: int):
	GLib.Thread.new(None, App.player.play_queue, queue, index)


def _on_seek_in_song(_, __, target: float):
	App.player.seek(target)


# swap songs in playlist event
def _on_swap_in_playlist(btn: Gtk.Button, i: int, j: int, playlist: str):
	btn.get_parent().get_parent().get_parent().popdown()
	myuzi.playlists.swap_songs(i, j, playlist)
	build_playlists()
	build_add_current_to_menu()


# add song to playlist event
def _on_add_to_playlist(btn: Gtk.Button, id_: str, title: str, list_: str):
	btn.get_parent().get_parent().get_parent().popdown()
	myuzi.playlists.add_song(id_, title, list_)
	build_playlists()
	build_add_current_to_menu()


# add currently playing song to playlist
def _on_add_current_to_playlist(btn: Gtk.Button, list_: str):
	btn.get_parent().get_parent().get_parent().popdown()

	song = App.player.get_current_song()
	if song:
		myuzi.playlists.add_song(song['id'], song['title'], list_)

	build_playlists()
	build_add_current_to_menu()


# add currently playing song
def _on_add_current_to_new_playlist(btn: Gtk.Button):
	song = App.player.get_current_song()
	if song:
		_on_create_playlist(btn, song)
	else:
		_on_create_playlist(btn)

	build_playlists()
	build_add_current_to_menu()


# rename song in playlist event
def _on_rename_song(btn: Gtk.Button, name: str, index: int, playlist: str):
	btn.get_parent().get_parent().get_parent().popdown()

	def _on_popup_done(new_name: str):
		nonlocal index, playlist
		myuzi.playlists.rename_song(index, playlist, new_name)
		build_playlists()
		build_add_current_to_menu()

	show_rename_popup(_on_popup_done, name)


# save song to local storage event
def _on_cache_song(btn: Gtk.Button, id_: str):
	btn.get_parent().get_parent().get_parent().popdown()

	Interface.window.set_sensitive(False)
	Interface.window.set_title(_('Downloading song...'))
	App.downloading = True

	def post_cache_unlock(id_: str):
		# done
		if not App.downloading:
			# download failed
			if not myuzi.cache.is_song_cached(id_):
				msg_dialog = Gtk.MessageDialog(
					transient_for = Interface.window,
					destroy_with_parent = True,
					modal = True,
					buttons = Gtk.ButtonsType.OK,
					text = _('Download error'),
					secondary_text = _('Song download failed')
				)
				msg_dialog.connect('response', lambda d, _: d.destroy())
				msg_dialog.show()

			# cleanup
			Interface.window.set_title('Myuzi')
			Interface.window.set_sensitive(True)
			build_playlists()
			build_add_current_to_menu()
			return False

		return True

	GLib.Thread.new('cache thread', myuzi.cache.cache_song, id_, App)
	GLib.timeout_add_seconds(1, post_cache_unlock, id_)


# remove song from local storage event
def _on_uncache_song(btn: Gtk.Button, id_: str):
	btn.get_parent().get_parent().get_parent().popdown()
	myuzi.cache.uncache_song(id_)
	build_playlists()
	build_add_current_to_menu()


# remove song from playlist event
def _on_remove_from_playlist(btn: Gtk.Button, index: int, playlist: str):
	btn.get_parent().get_parent().get_parent().popdown()

	song_id = myuzi.playlists.read_playlists()[playlist][index]['id']
	myuzi.playlists.remove_song(index, playlist)

	# only uncache if the song is not in any other playlists
	if not myuzi.playlists.is_song_in_any_playlist(song_id):
		myuzi.cache.uncache_song(song_id)

	build_playlists()
	build_add_current_to_menu()


# create playlist with song event
def _on_create_playlist(btn: Gtk.Button, song: dict = None):
	btn.get_parent().get_parent().get_parent().popdown()

	def _on_popup_done(name: str):
		nonlocal song
		myuzi.playlists.add_playlist(name, songs = [song] if song else [])
		build_search()
		build_playlists()
		build_add_current_to_menu()

	show_rename_popup(_on_popup_done, '')


# import playlist event
def _on_import_playlist(btn: Gtk.Button):
	btn.get_parent().get_parent().get_parent().popdown()
	name = ''

	def do_import(clipboard: object, task: object):
		nonlocal name

		try:
			data = clipboard.read_text_finish(task)
			success = myuzi.playlists.import_playlist(name, data)
		except gi.repository.GLib.GError:
			success = False

		if success:
			build_playlists()
			build_add_current_to_menu()
		else:
			msg_dialog = Gtk.MessageDialog(
				transient_for = Interface.window,
				destroy_with_parent = True,
				modal = True,
				buttons = Gtk.ButtonsType.OK,
				text = _('Playlist import failed'),
				secondary_text = _('No playlist data or playlist URL in clipboard')
			)
			msg_dialog.connect('response', lambda d, _: d.destroy())
			msg_dialog.show()

	def _on_popup_done(new_name: str):
		nonlocal btn, name

		name = new_name
		btn.get_clipboard().read_text_async(callback = do_import)

	show_rename_popup(_on_popup_done, name)


# rename playlist event
def _on_rename_playlist(btn: Gtk.Button, name: str):
	btn.get_parent().get_parent().get_parent().popdown()

	def _on_popup_done(new_name: str):
		nonlocal name
		if myuzi.playlists.rename_playlist(name, new_name):
			build_playlists()
			build_add_current_to_menu()
		else:
			msg_dialog = Gtk.MessageDialog(
				transient_for = Interface.window,
				destroy_with_parent = True,
				modal = True,
				buttons = Gtk.ButtonsType.OK,
				text = _('Playlist rename failed'),
				secondary_text = _('Playlist already exists')
			)

			msg_dialog.connect('response', lambda d, _: d.destroy())
			msg_dialog.show()

	show_rename_popup(_on_popup_done, name)


# share playlist event
def _on_share_playlist(btn: Gtk.Button, name: str):
	btn.get_parent().get_parent().get_parent().popdown()

	btn.get_clipboard().set_content(
		Gdk.ContentProvider.new_for_value(myuzi.playlists.export_playlist(name))
	)
	msg_dialog = Gtk.MessageDialog(
		transient_for = Interface.window,
		destroy_with_parent = True,
		modal = True,
		buttons = Gtk.ButtonsType.OK,
		text = _('Exported'),
		secondary_text = _('Playlist data copied to clipboard')
	)
	msg_dialog.connect('response', lambda d, _: d.destroy())
	msg_dialog.show()


# expand playlist event
def _on_expand_playlist(exp: Gtk.Expander, name: str):
	if exp.get_expanded():
		if name not in Interface.collapsed_playlists:
			Interface.collapsed_playlists.append(name)
	elif name in Interface.collapsed_playlists:
		Interface.collapsed_playlists.remove(name)


def _on_cache_playlist(btn: Gtk.Button, name: str):
	btn.get_parent().get_parent().get_parent().popdown()

	Interface.window.set_sensitive(False)
	Interface.window.set_title(_('Downloading playlist...'))
	App.downloading = True
	songs = myuzi.playlists.read_playlists()[name]

	def cache_progress(songs: list):
		# calculate progress
		cached = 0
		for song in songs:
			if myuzi.cache.is_song_cached(song['id']):
				cached += 1

		# done
		if cached == len(songs) or not App.downloading:
			# download failed?
			if cached != len(songs):
				msg_dialog = Gtk.MessageDialog(
					transient_for = Interface.window,
					destroy_with_parent = True,
					modal = True,
					buttons = Gtk.ButtonsType.OK,
					text = _('Download error'),
					secondary_text = _('Playlist download failed')
				)
				msg_dialog.connect('response', lambda d, _: d.destroy())
				msg_dialog.show()

			# cleanup
			Interface.window.set_title('Myuzi')
			build_playlists()
			build_add_current_to_menu()
			Interface.window.set_sensitive(True)
			return False

		return True

	GLib.Thread.new('cache thread', myuzi.cache.cache_playlist, name, App)
	GLib.timeout_add(400, cache_progress, songs)


def _on_uncache_playlist(btn: Gtk.Button, name: str):
	btn.get_parent().get_parent().get_parent().popdown()
	myuzi.cache.uncache_playlist(name)
	build_playlists()
	build_add_current_to_menu()


# delete playlist event
def _on_delete_playlist(btn: Gtk.Button, name: str):
	btn.get_parent().get_parent().get_parent().popdown()

	playlist = myuzi.playlists.read_playlists()[name].copy()
	myuzi.playlists.remove_playlist(name)

	# uncache songs that only appear in this playlist
	for song in playlist:
		if not myuzi.playlists.is_song_in_any_playlist(song['id']):
			myuzi.cache.uncache_song(song['id'])

	build_playlists()
	build_add_current_to_menu()


# search event
def _on_search(entry):
	if not App.player.online:
		msg_dialog = Gtk.MessageDialog(
			transient_for = Interface.window,
			destroy_with_parent = True,
			modal = True,
			buttons = Gtk.ButtonsType.OK,
			text = _('Unavailable'),
			secondary_text = _('Can\'t search in offline mode')
		)
		msg_dialog.connect('response', lambda d, _: d.destroy())
		msg_dialog.show()
		return

	def do_search(query: str):
		App.search_results = myuzi.utils.find_songs(query)
		App.searching = False

	def search_progress() -> bool:
		if App.searching:
			return True

		build_search()
		Interface.window.set_sensitive(True)
		Interface.window.set_title('Myuzi')
		return False

	Interface.window.set_title(_('Searching...'))
	Interface.window.set_sensitive(False)
	App.searching = True
	GLib.Thread.new('search thread', do_search, entry.get_text())
	GLib.timeout_add_seconds(1, search_progress)


def _on_toggle_online(chk: Gtk.CheckButton):
	App.player.online = False

	if chk.get_active():
		if myuzi.utils.is_yt_available():
			App.player.online = True
		else:
			chk.set_active(False)
			msg_dialog = Gtk.MessageDialog(
				transient_for = Interface.window,
				destroy_with_parent = True,
				modal = True,
				buttons = Gtk.ButtonsType.OK,
				text = _('Can\'t switch to online mode'),
				secondary_text = _('Connection failed')
			)
			msg_dialog.connect('response', lambda d, _: d.destroy())
			msg_dialog.show()

	build_search()
	build_playlists()
	build_add_current_to_menu()


def _on_show_about(btn: Gtk.Button):
	btn.get_parent().get_parent().get_parent().popdown()

	win_about = Adw.AboutWindow.new()
	win_about.set_application_icon('com.gitlab.zehkira.Myuzi')
	win_about.set_application_name('Myuzi')
	win_about.set_version(__version__)
	win_about.set_copyright('Copyright © 2022 zehkira')
	win_about.set_license_type(Gtk.License.CUSTOM)
	win_about.set_license(
'''Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.'''
	)
	win_about.set_translator_credits(_('translator-credits'))
	win_about.add_credit_section(_('Patrons'), ['yuanca'])
	win_about.set_issue_url('https://gitlab.com/zehkira/myuzi/-/issues')
	win_about.add_link(_('Donate'), 'https://www.patreon.com/bePatron?u=65739770')
	win_about.set_website('https://gitlab.com/zehkira/myuzi')
	win_about.set_transient_for(Interface.window)
	win_about.show()


# switch tab event
def _on_switch_page(_, page, page_num):
	if page_num == 0:
		build_playlists()
	elif page_num == 1:
		build_search()


# volume change event
def _on_volume_change(scale: Gtk.Scale):
	volume = scale.get_value()
	App.player.set_volume(volume)
	myuzi.settings.set_value('volume', volume)


# radio autoplay toggled event
def _on_radio_toggle(toggle: Gtk.CheckButton):
	myuzi.settings.set_value('radio', int(toggle.get_active()))


# add to queue
def _on_queue_song(btn: Gtk.Button, song: dict):
	btn.get_parent().get_parent().get_parent().popdown()
	App.player.queue_song(song)


# remove from queue
def _on_unqueue_song(btn: Gtk.Button):
	btn.get_parent().get_parent().get_parent().popdown()
	App.player.unqueue_song()


def _on_copy_song_url(btn: Gtk.Button):
	btn.get_parent().get_parent().get_parent().popdown()

	song = App.player.get_current_song()
	if song:
		btn.get_clipboard().set_content(
			Gdk.ContentProvider.new_for_value(
				'https://www.youtube.com/watch?v=' + song['id']
			)
		)

def _on_shuffle_toggle(toggle: Gtk.ToggleButton):
	App.player.shuffle = toggle.get_active()


def _on_loop_toggle(toggle: Gtk.ToggleButton):
	App.player.loop = toggle.get_active()


### --- INTERFACE BUILDERS --- ###


# (re)builds search tab interface
def build_search():
	# clear old results
	while True:
		child = Interface.box_results.get_first_child()
		if child:
			Interface.box_results.remove(child)
		else:
			break

	# write new results (don't ask yt every time!)
	for song in App.search_results:
		# clickable song title (plays song)
		btn_song = Gtk.Button.new()
		btn_song.set_hexpand(True)
		btn_song.set_has_frame(False)

		# thumbnail
		img_thumb = Gtk.Image.new_from_file(
			os.getenv(
				'XDG_CONFIG_HOME',
				os.path.expanduser('~/.config')
			) + '/myuzi/thumbs/' + song['id']
		)
		img_thumb.set_vexpand(True)
		img_thumb.set_valign(Gtk.Align.FILL)
		img_thumb.set_pixel_size(48)

		esc_title = GLib.markup_escape_text(song['title'], -1)
		esc_author = GLib.markup_escape_text(song['author'], -1)
		esc_length = GLib.markup_escape_text(song['length'], -1)
		lbl_song = Gtk.Label.new('')
		lbl_song.set_markup(
			f'{esc_title}\n<span fgalpha="66%">' +
			f'{esc_length}  {esc_author}</span>'
		)
		lbl_song.set_halign(Gtk.Align.START)
		lbl_song.set_ellipsize(Pango.EllipsizeMode.END)

		box_btn_song = Gtk.Box(orientation = Gtk.Orientation.HORIZONTAL)
		box_btn_song.set_spacing(10)
		box_btn_song.append(img_thumb)
		box_btn_song.append(lbl_song)

		btn_song.set_child(box_btn_song)
		btn_song.connect('clicked', _on_play_queue, [song], 0)

		if not App.player.online and not myuzi.cache.is_song_cached(song['id']):
			btn_song.set_sensitive(False)

		# add to playlist menu
		box_pop = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
		box_pop.set_spacing(5)
		for playlist in myuzi.playlists.read_playlists().keys():
			btn_playlist = Gtk.Button.new_with_label(playlist)
			btn_playlist.get_first_child().set_ellipsize(Pango.EllipsizeMode.END)
			btn_playlist.set_has_frame(False)
			btn_playlist.connect(
				'clicked',
				_on_add_to_playlist,
				song['id'],
				song['title'],
				playlist
			)
			box_pop.append(btn_playlist)

		box_pop.prepend(Gtk.Separator.new(Gtk.Orientation.HORIZONTAL))
		btn_new = Gtk.Button.new_with_label(_('New playlist...'))
		btn_new.set_has_frame(False)
		btn_new.connect('clicked', _on_create_playlist, song)
		box_pop.prepend(btn_new)

		# add to queue
		btn_queue = Gtk.Button.new_with_label(_('Add to queue'))
		btn_queue.set_has_frame(False)
		btn_queue.connect('clicked', _on_queue_song, song)
		box_pop.prepend(btn_queue)

		pop_add = Gtk.Popover.new()
		pop_add.set_child(box_pop)

		# add to playlist button
		btn_add = Gtk.MenuButton()
		btn_add.set_has_frame(False)
		btn_add.set_icon_name('list-add')
		btn_add.set_popover(pop_add)

		# box for single song
		box_result = Gtk.Box(orientation = Gtk.Orientation.HORIZONTAL)
		box_result.set_spacing(5)
		box_result.set_hexpand(True)
		box_result.set_halign(Gtk.Align.FILL)
		box_result.append(btn_song)
		box_result.append(btn_add)
		Interface.box_results.append(box_result)


# (re)builds playlists tab interface
def build_playlists():
	# for use with Interface.collapsed_playlists. widget:playlist_name
	hideables = {}

	# box for playlists
	box_lists = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
	box_lists.set_spacing(5)
	box_lists.set_margin_top(10)
	box_lists.set_margin_bottom(10)
	box_lists.set_hexpand(True)

	# all playlists
	for name, content in myuzi.playlists.read_playlists().items():
		# expand playlist button
		exp_list = Gtk.Expander.new_with_mnemonic(name)
		exp_list.set_halign(Gtk.Align.FILL)
		exp_list.set_valign(Gtk.Align.START)
		exp_list.set_hexpand(True)
		exp_list.connect('activate', _on_expand_playlist, name)
		hideables[exp_list] = name

		box_exp = Gtk.Box(orientation = Gtk.Orientation.HORIZONTAL)
		box_exp.set_spacing(10)
		box_exp.set_halign(Gtk.Align.FILL)
		box_exp.set_hexpand(True)
		lbl_exp = Gtk.Label.new(name)
		lbl_exp.set_ellipsize(Pango.EllipsizeMode.END)
		lbl_exp.set_valign(Gtk.Align.CENTER)
		lbl_exp.set_halign(Gtk.Align.START)
		lbl_exp.set_hexpand(True)
		box_exp.append(lbl_exp)
		exp_list.set_label_widget(box_exp)
		box_lists.append(exp_list)

		# menu with more playlist actions
		box_pop = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
		box_pop.set_spacing(5)
		btn_delete = Gtk.Button.new_with_label(_('Delete'))
		btn_delete.set_has_frame(False)
		btn_delete.connect('clicked', _on_delete_playlist, name)
		btn_uncache = Gtk.Button.new_with_label(_('Remove from local storage'))
		btn_uncache.set_has_frame(False)
		btn_uncache.connect('clicked', _on_uncache_playlist, name)
		btn_share = Gtk.Button.new_with_label(_('Export to clipboard'))
		btn_share.set_has_frame(False)
		btn_share.connect('clicked', _on_share_playlist, name)
		btn_rename = Gtk.Button.new_with_label(_('Rename...'))
		btn_rename.set_has_frame(False)
		btn_rename.connect('clicked', _on_rename_playlist, name)
		box_pop.append(btn_rename)
		box_pop.append(btn_share)
		if App.player.online:
			btn_cache = Gtk.Button.new_with_label(_('Save to local storage'))
			btn_cache.set_has_frame(False)
			btn_cache.connect('clicked', _on_cache_playlist, name)
			box_pop.append(btn_cache)
		box_pop.append(btn_uncache)
		box_pop.append(btn_delete)

		pop_more = Gtk.Popover.new()
		pop_more.set_child(box_pop)

		# button for "more" menu
		btn_more = Gtk.MenuButton()
		btn_more.set_icon_name('view-more')
		btn_more.set_has_frame(False)
		btn_more.set_popover(pop_more)
		btn_more.set_halign(Gtk.Align.FILL)
		box_exp.append(btn_more)

		# box for all songs in playlist
		box_songs = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
		box_songs.set_spacing(5)
		box_songs.set_margin_top(5)

		# all playlist songs
		for i, song in enumerate(content):
			# clickable song title (plays playlist starting with song)
			btn_song = Gtk.Button.new_with_label(song['title'])
			btn_song.set_hexpand(True)
			btn_song.set_has_frame(False)
			btn_song.get_first_child().set_ellipsize(Pango.EllipsizeMode.END)
			btn_song.get_first_child().set_halign(Gtk.Align.START)
			btn_song.connect('clicked', _on_play_queue, content, i)

			# "more" menu - song actions
			box_pop = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
			box_pop.set_spacing(5)
			btn_rename = Gtk.Button.new_with_label(_('Rename...'))
			btn_rename.set_has_frame(False)
			btn_rename.connect('clicked', _on_rename_song, song['title'], i, name)
			box_pop.append(btn_rename)
			btn_queue = Gtk.Button.new_with_label(_('Add to queue'))
			btn_queue.set_has_frame(False)
			btn_queue.connect('clicked', _on_queue_song, song)
			box_pop.append(btn_queue)
			btn_up, btn_down, btn_uncache, btn_cache = None, None, None, None
			if i != 0:
				btn_up = Gtk.Button.new_with_label(_('Move up'))
				btn_up.set_has_frame(False)
				btn_up.connect('clicked', _on_swap_in_playlist, i, i - 1, name)
				box_pop.append(btn_up)
			if i != len(content) - 1:
				btn_down = Gtk.Button.new_with_label(_('Move down'))
				btn_down.set_has_frame(False)
				btn_down.connect('clicked', _on_swap_in_playlist, i, i + 1, name)
				box_pop.append(btn_down)
			if myuzi.cache.is_song_cached(song['id']):
				btn_uncache = Gtk.Button.new_with_label(_('Remove from local storage'))
				btn_uncache.set_has_frame(False)
				btn_uncache.connect('clicked', _on_uncache_song, song['id'])
				box_pop.append(btn_uncache)
			elif App.player.online:
				btn_cache = Gtk.Button.new_with_label(_('Save to local storage'))
				btn_cache.set_has_frame(False)
				btn_cache.connect('clicked', _on_cache_song, song['id'])
				box_pop.append(btn_cache)
			else:
				btn_song.set_sensitive(False)
			btn_remove = Gtk.Button.new_with_label(_('Remove from playlist'))
			btn_remove.set_has_frame(False)
			btn_remove.connect('clicked', _on_remove_from_playlist, i, name)
			box_pop.append(btn_remove)

			pop_more = Gtk.Popover.new()
			pop_more.set_child(box_pop)

			# button for "more" menu
			btn_more = Gtk.MenuButton()
			btn_more.set_has_frame(False)
			btn_more.set_icon_name('view-more')
			btn_more.set_popover(pop_more)

			# box for song
			box = Gtk.Box(orientation = Gtk.Orientation.HORIZONTAL)
			box.set_spacing(5)
			box.set_hexpand(True)
			box.set_halign(Gtk.Align.FILL)
			box.append(btn_more)
			box.append(btn_song)
			box_songs.append(box)

		exp_list.set_child(box_songs)

	clm_playlists = Adw.Clamp()
	clm_playlists.set_margin_start(10)
	clm_playlists.set_margin_end(10)
	clm_playlists.set_vexpand(True)
	clm_playlists.set_child(box_lists)

	Interface.scr_lists.set_child(clm_playlists)

	# playlists should stay collapsed
	for widget, name in hideables.items():
		if name in Interface.collapsed_playlists:
			widget.set_expanded(False)
		else:
			widget.set_expanded(True)


# what a mess.
def build_add_current_to_menu():
	# add to playlist menu
	box_pop = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
	box_pop.set_spacing(5)
	for playlist in myuzi.playlists.read_playlists().keys():
		btn_playlist = Gtk.Button.new_with_label(playlist)
		btn_playlist.get_first_child().set_ellipsize(Pango.EllipsizeMode.END)
		btn_playlist.set_has_frame(False)
		btn_playlist.connect('clicked', _on_add_current_to_playlist, playlist)
		box_pop.append(btn_playlist)

	box_pop.prepend(Gtk.Separator.new(Gtk.Orientation.HORIZONTAL))
	btn_new = Gtk.Button.new_with_label(_('New playlist...'))
	btn_new.set_has_frame(False)
	btn_new.connect('clicked', _on_add_current_to_new_playlist)
	box_pop.prepend(btn_new)

	pop_add = Gtk.Popover.new()
	pop_add.set_child(box_pop)
	Interface.btn_add_current_to.set_popover(pop_add)


# builds the app's interface
def build(app):
	logging.basicConfig(level = logging.INFO)

	## player
	# current song title
	Interface.lbl_title = Gtk.Label()
	Interface.lbl_title.set_halign(Gtk.Align.CENTER)
	Interface.lbl_title.set_ellipsize(Pango.EllipsizeMode.END)
	Interface.lbl_title.set_margin_top(10)
	Interface.lbl_title.set_margin_start(5)
	Interface.lbl_title.set_margin_end(5)
	Interface.lbl_title.set_margin_bottom(10)
	Interface.lbl_title.set_label('...')

	# song progress thing
	Interface.scl_progress = Gtk.Scale.new_with_range(
		Gtk.Orientation.HORIZONTAL, 0, 1, 0.01
	)
	Interface.scl_progress.set_draw_value(False)
	Interface.scl_progress.set_halign(Gtk.Align.FILL)
	Interface.scl_progress.set_valign(Gtk.Align.END)
	Interface.scl_progress.connect('change-value', _on_seek_in_song)
	GLib.timeout_add(100, update_state)

	# playback control buttons
	Interface.btn_pause = Gtk.Button.new_from_icon_name('media-playback-start')
	Interface.btn_pause.connect('clicked', _on_toggle_pause)
	Interface.btn_pause.set_has_frame(False)
	btn_next = Gtk.Button.new_from_icon_name('media-skip-forward')
	btn_next.connect('clicked', _on_next_song)
	btn_next.set_has_frame(False)
	btn_prev = Gtk.Button.new_from_icon_name('media-skip-backward')
	btn_prev.connect('clicked', _on_previous_song)
	btn_prev.set_has_frame(False)
	tog_loop = Gtk.ToggleButton()
	tog_loop.set_icon_name('media-playlist-repeat')
	tog_loop.set_has_frame(False)
	tog_loop.connect('toggled', _on_loop_toggle)
	tog_shuffle = Gtk.ToggleButton()
	tog_shuffle.set_icon_name('media-playlist-shuffle')
	tog_shuffle.set_has_frame(False)
	tog_shuffle.connect('toggled', _on_shuffle_toggle)

	# add to playlist button
	Interface.btn_add_current_to = Gtk.MenuButton()
	Interface.btn_add_current_to.set_has_frame(False)
	Interface.btn_add_current_to.set_icon_name('list-add')

	# remove from queue
	btn_unqueue = Gtk.Button.new_with_label(_('Remove from queue'))
	btn_unqueue.set_has_frame(False)
	btn_unqueue.connect('clicked', _on_unqueue_song)

	# copy url
	btn_url = Gtk.Button.new_with_label(_('Copy song URL'))
	btn_url.set_has_frame(False)
	btn_url.connect('clicked', _on_copy_song_url)

	# volume control
	lbl_volume = Gtk.Label.new(_('Volume'))
	scl_volume = Gtk.Scale.new_with_range(Gtk.Orientation.HORIZONTAL, 0, 1, 0.1)
	scl_volume.set_hexpand(True)
	scl_volume.set_draw_value(False)
	scl_volume.connect('value-changed', _on_volume_change)

	try:
		v = float(myuzi.settings.get_value('volume'))
		scl_volume.set_value(v)
	except ValueError:
		scl_volume.set_value(1)
		myuzi.settings.set_value('volume', 1)

	box_volume = Gtk.Box(orientation = Gtk.Orientation.HORIZONTAL)
	box_volume.set_spacing(5)
	box_volume.set_halign(Gtk.Align.FILL)
	box_volume.set_hexpand(True)
	box_volume.append(lbl_volume)
	box_volume.append(scl_volume)

	# autoplay radio checkbox
	chk_autoplay = Gtk.CheckButton.new_with_label(_('Autoplay similar songs after queue ends'))
	chk_autoplay.get_last_child().set_wrap(True)
	chk_autoplay.connect('toggled', _on_radio_toggle)

	try:
		v = bool(int(myuzi.settings.get_value('radio')))
		chk_autoplay.set_active(v)
	except ValueError:
		chk_autoplay.set_active(False)
		myuzi.settings.set_value('radio', 0)

	box_pop = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
	box_pop.set_spacing(5)
	box_pop.append(btn_unqueue)
	box_pop.append(btn_url)
	box_pop.append(Gtk.Separator.new(Gtk.Orientation.HORIZONTAL))
	box_pop.append(box_volume)
	box_pop.append(chk_autoplay)
	pop_more = Gtk.Popover.new()
	pop_more.set_child(box_pop)

	btn_more = Gtk.MenuButton()
	btn_more.set_icon_name('view-more')
	btn_more.set_popover(pop_more)
	btn_more.set_has_frame(False)

	# box for playback controls
	box_controls = Gtk.Box(orientation = Gtk.Orientation.HORIZONTAL)
	box_controls.set_spacing(2)
	box_controls.set_valign(Gtk.Align.END)
	box_controls.set_halign(Gtk.Align.CENTER)
	box_controls.append(Interface.btn_add_current_to)
	box_controls.append(tog_shuffle)
	box_controls.append(btn_prev)
	box_controls.append(Interface.btn_pause)
	box_controls.append(btn_next)
	box_controls.append(tog_loop)
	box_controls.append(btn_more)

	# metabox for player
	box_player = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
	box_player.append(Interface.lbl_title)
	box_player.append(box_controls)
	box_player.append(Interface.scl_progress)

	## search tab
	# search entry
	ent_search = Gtk.SearchEntry()
	ent_search.connect('activate', _on_search)

	clm_search = Adw.Clamp()
	clm_search.set_child(ent_search)

	# results box
	Interface.box_results = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
	Interface.box_results.set_margin_bottom(10)

	clm_results = Adw.Clamp()
	clm_results.set_child(Interface.box_results)

	scr_results = Gtk.ScrolledWindow()
	scr_results.set_vexpand(True)
	scr_results.set_child(clm_results)

	# search tab box
	box_search = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
	box_search.set_spacing(10)
	box_search.append(clm_search)
	box_search.append(scr_results)
	box_search.set_margin_start(10)
	box_search.set_margin_top(10)
	box_search.set_margin_end(10)
	box_search.set_vexpand(True)

	## playlists tab
	Interface.scr_lists = Gtk.ScrolledWindow()
	Interface.scr_lists.set_vexpand(True)
	Interface.scr_lists.set_hexpand(True)

	## stack for switching
	stk_main = Gtk.Stack()
	stk_main.add_titled(Interface.scr_lists, 'playlists', _('Playlists'))
	stk_main.add_titled(box_search, 'search', _('Search'))

	ssw_main = Gtk.StackSwitcher()
	ssw_main.set_stack(stk_main)

	# online mode toggle
	chk_online = Gtk.CheckButton.new_with_label(_('Online'))
	chk_online.set_active(False)
	chk_online.connect('toggled', _on_toggle_online)

	# import button
	btn_import = Gtk.Button.new_with_label(_('Import from clipboard'))
	btn_import.set_has_frame(False)
	btn_import.connect('clicked', _on_import_playlist)

	# about button
	btn_about = Gtk.Button.new_with_label(_('About'))
	btn_about.set_has_frame(False)
	btn_about.connect('clicked', _on_show_about)

	# extra misc menu
	box_pop = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
	box_pop.set_spacing(5)
	box_pop.append(btn_about)
	box_pop.append(btn_import)
	box_pop.append(Gtk.Separator.new(Gtk.Orientation.HORIZONTAL))
	box_pop.append(chk_online)
	pop_more = Gtk.Popover.new()
	pop_more.set_child(box_pop)

	btn_more = Gtk.MenuButton()
	btn_more.set_icon_name('view-more')
	btn_more.set_popover(pop_more)

	## CSD - titlebar
	hbr_topbar = Adw.HeaderBar()
	hbr_topbar.set_title_widget(ssw_main)
	hbr_topbar.pack_end(btn_more)

	## toplevels
	box_main = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
	box_main.append(hbr_topbar)
	box_main.append(stk_main)
	box_main.append(Gtk.Separator.new(Gtk.Orientation.HORIZONTAL))
	box_main.append(box_player)

	# app window
	Interface.window = Adw.ApplicationWindow(application = app, title = 'Myuzi')
	Interface.window.set_default_size(600, 400)
	Interface.window.set_content(box_main)
	Interface.window.connect('close-request', _on_exit)

	build_playlists()
	build_add_current_to_menu()
	Interface.window.show()

	# try to enter online mode right away
	def try_online():
		App.player.online = myuzi.utils.is_yt_available()

	def online_check_progress() -> bool:
		if App.player.online is None:
			return True

		chk_online.set_active(App.player.online)
		build_search()
		build_playlists()
		build_add_current_to_menu()
		Interface.window.set_sensitive(True)
		return False

	Interface.window.set_sensitive(False)
	GLib.Thread.new('online thread', try_online)
	GLib.timeout_add(100, online_check_progress)
