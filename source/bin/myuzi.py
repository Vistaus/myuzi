#!/usr/bin/env python3

import gettext, os

import myuzi.app

import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gio, Adw


### --- MAIN --- ###


def activate(app: Adw.Application):
	myuzi.app.build(app)


def main():
	# pick translation
	lang = os.getenv('LANG', 'en_US.UTF-8')
	chosen_lang = 'en'
	for l in ['en', 'de', 'pl', 'sv', 'it', 'fr', 'ru']: # supported languages
		if lang.split('_')[0] == l:
			chosen_lang = l
			break

	# setup.py --prefix -> sys.base_prefix -> autodetection of locales dir
	# ..except it doesn't work with flatpak.
	if os.getenv('container', '') != 'flatpak':
		gettext.translation('myuzi', languages = [chosen_lang]).install()
	else:
		gettext.translation(
			'myuzi',
			localedir = '/app/share/locale',
			languages = [chosen_lang]
		).install()

	app = Adw.Application.new(
		'com.gitlab.zehkira.Myuzi',
		Gio.ApplicationFlags.DEFAULT_FLAGS
	)
	app.connect('activate', activate)
	app.run()


if __name__ == '__main__':
	main()

