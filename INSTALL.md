| [Install from Flathub](https://flathub.org/apps/details/com.gitlab.zehkira.Myuzi) | [Install from the AUR](https://aur.archlinux.org/packages/myuzi) |
|-|-|

Installation from source is also possible:

Install these dependencies:
- [Gtk 4](https://www.gtk.org/)
    - Libadwaita
- [yt-dlp](https://github.com/yt-dlp/yt-dlp)
- [GStreamer](https://github.com/GStreamer/gstreamer)
    - [gst-libav](https://github.com/GStreamer/gst-libav)
    - [gst-plugins-good](https://github.com/GStreamer/gst-plugins-good)
- [Python 3](https://www.python.org/) (with pip)
    - [Nuitka](https://nuitka.net/)
    - [BeautifulSoup4](https://pypi.org/project/beautifulsoup4/)
    - [PyGObject](https://pygobject.readthedocs.io/en/latest/getting_started.html#getting-started)
    - [Requests](https://pypi.org/project/requests/)

Install the app:

```
$ git clone https://gitlab.com/zehkira/myuzi.git
$ cd myuzi/source
# make install
```

Unistall the app:

```
# make uninstall
```
