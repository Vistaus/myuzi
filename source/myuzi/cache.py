import glob, os, subprocess

import myuzi.playlists

from gi.repository import GLib


def is_song_cached(video_id: str) -> bool:
	return os.path.exists(
		GLib.get_user_special_dir(
			GLib.UserDirectory.DIRECTORY_MUSIC
		) + '/myuzi/' + video_id
	)


# saves song to local storage
def cache_song(video_id: str, App = None):
	# don't overwrite. yt video content doesn't change
	if is_song_cached(video_id):
		return

	path = GLib.get_user_special_dir(
		GLib.UserDirectory.DIRECTORY_MUSIC
	) + '/myuzi/'
	os.makedirs(path, exist_ok = True)

	out, _ = subprocess.Popen(
		f'yt-dlp -x --no-cache-dir --audio-quality 0 --sponsorblock-remove music_offtopic --add-metadata -o "{path}/%(id)s.%(ext)s" https://www.youtube.com/watch?v={video_id}',
		shell = True,
		stdout = subprocess.PIPE
	).communicate()

	# rename id.* files to id
	for file in glob.glob(path + '/*.*'):
		os.rename(file, '.'.join(file.split('.')[:-1]))

	# notify App we're done
	if App:
		App.downloading = False


# removes song from local storage
def uncache_song(video_id: str):
	try:
		os.remove(
			GLib.get_user_special_dir(
				GLib.UserDirectory.DIRECTORY_MUSIC
			) + '/myuzi/' + video_id
		)
	except FileNotFoundError:
		pass


def cache_playlist(name: str, App):
	for song in myuzi.playlists.read_playlists()[name]:
		cache_song(song['id'])

	# notify App we're done
	App.downloading = False


def uncache_playlist(name: str):
	for song in myuzi.playlists.read_playlists()[name]:
		uncache_song(song['id'])
