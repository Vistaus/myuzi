��    )      d  ;   �      �     �     �  '   �     �     �               -     4     ;     J     b     v     �     �     �  	   �     �     �  -   �                    %  !   =     _     x     �  	   �     �     �     �  	   �     �               8     E     Z     f  B  m     �  "   �  m   �  W   W  J   �  #   �  %   	     D	     S	     l	  ?   �	     �	  "   �	  )   
     6
  +   S
     
  !   �
     �
  �   �
     �     �  +   �  G   �  j   )  7   �  C   �  ?        P  ;   c  ?   �  "   �       ;      
   \  >   g     �  /   �     �     �            #   "          &              	       (                                           %      
   $   )                                !                                                  '                About Add to queue Autoplay similar songs after queue ends Can't search in offline mode Can't switch to online mode Connection failed Copy song URL Delete Donate Download error Downloading playlist... Downloading song... Enter name... Export to clipboard Exported Import from clipboard Move down Move up New playlist... No playlist data or playlist URL in clipboard Online Patrons Playback error Playlist already exists Playlist data copied to clipboard Playlist download failed Playlist import failed Playlist rename failed Playlists Remove from local storage Remove from playlist Remove from queue Rename... Save to local storage Search Searching for a song to play... Searching... Song download failed Unavailable Volume Project-Id-Version: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Generator: Poedit 3.2.1
 О Программе Добавить в очередь Автовоспроизведение похожих песен после окончания очереди Невозможно выполнить поиск в автономном режиме Невозможно переключиться в онлайн-режим Ошибка подключения Скопировать URL песни Удалить Пожертвовать Ошибка загрузки Загрузка списка воспроизведения... Загрузка песни... Введите название... Экспорт в буфер обмена Экспортировано Импорт из буфера обмена Переместить вниз Переместить вверх Новый плейлист... Отсутствуют данные списка воспроизведения или URL-адреса списка воспроизведения в буфере обмена Онлайн Покровители Ошибка воспроизведения Список воспроизведения уже существует Данные списка воспроизведения скопированы в буфер обмена Не удалось загрузить плейлист Сбой импорта списка воспроизведения Не удалось переименовать плейлист Плейлисты Удалить из локального хранилища Удалить из списка воспроизведения Удалить из очереди Переименовать... Сохранить в локальное хранилище Поиск Поиск песни для воспроизведения... Поиск... Загрузка песни не удалась Недоступно Громкость 