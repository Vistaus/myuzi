import json, os

import requests


def query_api_segments(video_id: str) -> dict | None:
	api = 'https://sponsor.ajay.app/api/skipSegments'
	segments = {}
	while True:
		try:
			text = requests.get(
				f'{api}?videoID={video_id}&category=music_offtopic&action=skip',
				timeout = 1
			).text

			# there can be any number of them
			for seg in json.loads(text):
				timestamps = seg['segment']
				segments[float(timestamps[0])] = float(timestamps[1])

			# dont bother saving empty
			if segments:
				write_local_segments(video_id, segments)

			return segments
		except (
			requests.exceptions.ConnectionError,
			requests.exceptions.ReadTimeout,
			json.decoder.JSONDecodeError,
			KeyError
		):
			return None


def write_local_segments(video_id: str, segments: dict):
	path = os.getenv('XDG_CONFIG_HOME', os.path.expanduser('~/.config')) + '/myuzi/skips'
	os.makedirs(path, exist_ok = True)

	with open(path + '/' + video_id, 'w') as skips_file:
		skips_file.write(json.dumps(segments))


def read_local_segments(video_id: str) -> dict | None:
	path = os.getenv('XDG_CONFIG_HOME', os.path.expanduser('~/.config')) + '/myuzi/skips/' + video_id

	try:
		with open(path, 'r') as skips_file:
			return {float(x): float(y) for x, y in json.loads(skips_file.read()).items()}
	except OSError:
		return None


def get_segments(video_id: str) -> dict | None:
	segments = read_local_segments(video_id)

	# both {} and None are good reasons to call the api
	if not segments:
		segments = query_api_segments(video_id)

	return segments

