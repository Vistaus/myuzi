��    (      \  5   �      p     q     w  '   �     �     �     �     �     �               ,     @     T     ]  	   d     n     v     �     �     �     �     �     �  !   �          .     E  	   \     f     �     �     �     �     �     �     �                   ,  	   K     U  P   s  +   �  '   �       	   ,     6     C  *   ^     �     �     �     �  	   �     �     �  9   	     @	  	   I	     S	  !   e	  -   �	  ?   �	  0   �	  ,   &
  *   S
     ~
     �
      �
      �
  "   �
  
     "        ?  #   L     p     }     �     	                                                                       $                   #      
      '      %   "   !                         (                            &    About Add to queue Autoplay similar songs after queue ends Can't search in offline mode Can't switch to online mode Connection failed Delete Donate Download error Downloading playlist... Downloading song... Export to clipboard Exported Import Move down Move up New playlist... No playlist data in clipboard Online Patrons Playback error Playlist already exists Playlist creation failed Playlist data copied to clipboard Playlist download failed Playlist import failed Playlist rename failed Playlists Remove from local storage Remove from playlist Remove from queue Save to local storage Search Searching for a song to play... Searching... Song download failed Unavailable Volume translator-credits Project-Id-Version: 
PO-Revision-Date: 
Last-Translator: Irénée Thirion <irenee.thirion@e.email>
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.1.1
 À propos Ajoute à la file d’attente Jouer automatiquement des titres similaires après la fin de la file d’attente Impossible de rechercher en mode hors-ligne Impossible de basculer en mode en ligne Connexion échouée Supprimer Faire un don Erreur de téléchargement Téléchargement de la liste de lecture... Téléchargement du titre... Exporter vers le presse-papiers Exporté Importer Descendre Monter Nouvelle liste de lecture... Aucune donnée de liste de lecture dans le presse-papiers En ligne Donateurs Erreur de lecture La liste de lecture existe déjà Échec de la création de la liste de lecture Données de la liste de lecture copiées dans le presse-papiers Téléchargement de la liste de lecture échoué Importation de la liste de lecture échouée Échec du renommage de la liste de lecture Listes de lecture Supprimer du stockage local Supprimer de la liste de lecture Supprimer de la file d’attente Enregistrer dans le stockage local Rechercher Recherche d’un titre à jouer... Recherche... Échec du téléchargement du titre Indisponible Volume Irénée THIRION 